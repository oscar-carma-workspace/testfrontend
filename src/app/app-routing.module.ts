import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidTokenGuard } from './guard/valid-token.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./ui/auth/auth.module').then( m => m.AuthModule )
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./ui/protected/protected.module').then( m => m.ProtectedModule ),
    canActivate: [ ValidTokenGuard ],
    canLoad: [ ValidTokenGuard ]
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: false
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
