import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ColorPickerModule } from '@iplab/ngx-color-picker';

import { ProtectedRoutingModule } from './protected-routing.module';
import { GifsComponent } from './pages/gifs/gifs.component';
import { SearchComponent } from './components/search/search.component';
import { ResultComponent } from './components/result/result.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { ColorComponent } from './pages/color/color.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';


@NgModule({
  declarations: [GifsComponent, SearchComponent, ResultComponent, SidebarComponent, ColorComponent, DashboardComponent],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    HttpClientModule,
    ColorPickerModule
  ],
  exports: [GifsComponent]
})
export class ProtectedModule { }
