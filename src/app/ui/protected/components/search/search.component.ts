import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifsService } from './../../service/gifs.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @ViewChild('txtSearch', {static: false}) txtSearch!:ElementRef<HTMLInputElement>;

  constructor( private GifsService: GifsService ) {}

  public search() {
    const v = this.txtSearch.nativeElement;

    this.GifsService.searchGifs( v.value );
    v.value = '';
  }

}
