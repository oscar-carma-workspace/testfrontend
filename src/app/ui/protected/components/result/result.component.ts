import { Component } from '@angular/core';
import { GifsService } from './../../service/gifs.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent {

  constructor(private gifsService: GifsService) { }

  get result() {
    return this.gifsService.result;
  }
}
