import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GifsService } from './../../service/gifs.service';
import { AuthService } from './../../../auth/service/auth.service';
import { ColorService } from '../../service/color.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

  constructor( private gifsService: GifsService, private router: Router, private authService: AuthService, public colorService: ColorService ) {
  }

  get history() {
    return this.gifsService.history;
  }

  public search( query: string ) {
    this.gifsService.searchGifs(query);
  }

  logout() {
    this.router.navigateByUrl('/auth');
    this.authService.logout();
  }

  color() {
    this.router.navigateByUrl('/dashboard/color');
    this.colorService.canvas = true;
  }

  return() {
    this.router.navigateByUrl('/dashboard/gifs');
    this.colorService.canvas = false;
    sessionStorage.setItem('color', this.colorService.color)
  }


}
