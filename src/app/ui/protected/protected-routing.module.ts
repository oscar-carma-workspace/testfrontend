import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GifsComponent } from './pages/gifs/gifs.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ColorComponent } from './pages/color/color.component';


const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'gifs', component: GifsComponent },
      { path: 'color', component: ColorComponent },
      { path: '**', redirectTo: 'gifs' },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
exports: [RouterModule]
})
export class ProtectedRoutingModule { }
