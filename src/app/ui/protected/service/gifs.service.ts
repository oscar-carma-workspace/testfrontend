import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Gif, SearchGifsResponse } from '../interface/gifs';

@Injectable({
  providedIn: 'root'
})
export class GifsService {
  private apiKey: string = 'wdXalnaSJi8dzz3jJP0yzmrKWYBpoTEq';
  private ghipyUrl: string = 'https://api.giphy.com/v1/gifs';
  private _history: string[] = [];

  public result: Gif[] = [];

  get history() {
    return [...this._history];
  }

  constructor(private http: HttpClient) {
    this._history = JSON.parse(localStorage.getItem('history')!) || [];
    this.result = JSON.parse(localStorage.getItem('result')!) || [];
    // if( localStorage.getItem('history')) {
    //   this._history = JSON.parse(localStorage.getItem('history')!);
    // }
    // this.searchGifs(localStorage.getItem('lastSearch')! || '');
  }

  public searchGifs(query: string) {
    query = query.trim().toLocaleLowerCase();

    if (query.length === 0) {
      return;
    } else if (!this._history.includes(query)) {
      this._history.unshift(query);
      this._history = this._history.splice(0, 10);

      localStorage.setItem('history', JSON.stringify(this._history));
      // localStorage.setItem('ultimoBuscado', query);
    }

    const params = new HttpParams()
      .set('api_key', this.apiKey)
      .set('q', query)
      .set('limit', '10');

      this.http.get<SearchGifsResponse>(`${ this.ghipyUrl }/search`, { params })
      .subscribe((resp) => {
        this.result = resp.data;
        localStorage.setItem('result', JSON.stringify(this.result));
      });
  }
}
