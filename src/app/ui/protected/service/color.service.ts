import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorService {
  color: string = sessionStorage.getItem('color') || '#212529';
  canvas: boolean = false;

constructor() { }

}
