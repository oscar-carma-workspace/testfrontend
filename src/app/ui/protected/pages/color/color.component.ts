import { Component, OnInit } from '@angular/core';
import { ColorService } from '../../service/color.service';


@Component({
  selector: 'app-color',
  templateUrl: './color.component.html',
  styleUrls: ['./color.component.scss']
})
export class ColorComponent implements OnInit {

  public color: string;

  constructor( private colorService: ColorService) {
    this.color = sessionStorage.getItem('color') || '#212529';
  }

  ngOnInit() {
  }

  changeColor() {
    // sessionStorage.setItem('color', this.color );
    this.colorService.color = this.color;
  }

}
